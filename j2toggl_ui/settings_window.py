import os
from typing import Optional

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QLineEdit, QPushButton, QComboBox
from PyQt5.uic import loadUi

from j2toggl_ui.resouces import qInitResources
from j2toggl_core.configuration.config import Config


class SettingsWindow(QDialog):
    DayOfWeek = {
        1: 0,
        7: 1,
    }

    def __init__(self, config: Config):
        super().__init__()

        qInitResources()

        self.config = config

        self.firstWeekDayComboBox: Optional[QComboBox] = None

        self.jiraHostLineEdit: Optional[QLineEdit] = None
        self.jiraUserLineEdit: Optional[QLineEdit] = None
        self.jiraTokenLineEdit: Optional[QLineEdit] = None

        self.tempoTokenlineEdit: Optional[QLineEdit] = None

        self.togglHostLineEdit: Optional[QLineEdit] = None
        self.togglUserLineEdit: Optional[QLineEdit] = None
        self.togglTokenLineEdit: Optional[QLineEdit] = None

        self.saveButton: Optional[QPushButton] = None
        self.cancelButton: Optional[QPushButton] = None
        self.applyButton: Optional[QPushButton] = None

        self._load_ui()
        self._init_widgets()
        self._init_signals()

        self._load_config()

        self.setWindowTitle("Settings")
        self.setWindowIcon(QIcon(':app-icon'))

    def _load_ui(self):
        current_path = os.path.dirname(__file__)
        ui_path = os.path.join(current_path, "ui", "settings.ui")

        loadUi(ui_path, self)

    def _init_widgets(self):
        self.firstWeekDayComboBox.addItem("Monday", 1)
        self.firstWeekDayComboBox.addItem("Sunday", 7)

    def _init_signals(self):
        self.cancelButton.clicked.connect(self._close)
        self.saveButton.clicked.connect(self._save)
        self.applyButton.clicked.connect(self._apply)

    def _load_config(self):
        weekDayIndex = self.DayOfWeek[self.config.first_date_of_week]
        self.firstWeekDayComboBox.setCurrentIndex(weekDayIndex)

        self.config.first_date_of_week = self.firstWeekDayComboBox.currentData()

        jiraConfig = self.config.jira
        if jiraConfig.host is not None:
            self.jiraHostLineEdit.setText(self.config.jira.host)
        if jiraConfig.user is not None:
            self.jiraUserLineEdit.setText(self.config.jira.user)
        if jiraConfig.token is not None:
            self.jiraTokenLineEdit.setText(self.config.jira.token)

        tempoConfig = self.config.tempo
        if tempoConfig.token is not None:
            self.tempoTokenlineEdit.setText(tempoConfig.token)

        togglConfig = self.config.toggl
        if togglConfig.host is not None:
            self.togglHostLineEdit.setText(togglConfig.host)
        if togglConfig.user_agent is not None:
            self.togglUserLineEdit.setText(togglConfig.user_agent)
        if togglConfig.token is not None:
            self.togglTokenLineEdit.setText(togglConfig.token)

    def _save(self):
        self._apply()
        self._close()

    def _apply(self):
        self._save_config()

    def _close(self):
        self.close()

    def _save_config(self):
        self.config.first_date_of_week = self.firstWeekDayComboBox.currentData()

        jiraConfig = self.config.jira
        jiraConfig.host = self.jiraHostLineEdit.text()
        jiraConfig.user = self.jiraUserLineEdit.text()
        jiraConfig.token = self.jiraTokenLineEdit.text()

        tempoConfig = self.config.tempo
        tempoConfig.token = self.tempoTokenlineEdit.text()

        togglConfig = self.config.toggl
        togglConfig.host = self.togglHostLineEdit.text()
        togglConfig.user_agent = self.togglUserLineEdit.text()
        togglConfig.token = self.togglTokenLineEdit.text()

        self.config.save()
