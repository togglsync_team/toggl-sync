import json
from pathlib import Path

import jsonschema
import os

from loguru import logger

from j2toggl_core.app_paths import get_app_file_path, get_app_directory_path
from j2toggl_core.configuration.config import Config

CONFIG_FILE_NAME = "app-config.json"


class JsonConfig(Config):
    # See http://json-schema.org/latest/json-schema-validation.html to update schema
    _configSchema = {
        "type": "object",
        "properties": {
            "application": {
                "type": "object",
                "properties": {
                    "firstDateOfWeek": {
                        "type": "integer",
                        "minimum": 1,
                        "maximum": 7
                    }
                }
            },
            "jira": {
                "type": "object",
                "properties": {
                    "host": {"type": "string"},
                    "user": {"type": "string"},
                    "token": {"type": "string"},
                }
            },
            "tempo": {
                "type": "object",
                "properties": {
                    "token": {"type": "string"},
                }
            },
            "toggl": {
                "type": "object",
                "properties": {
                    "host": {"type": "string"},
                    "user_agent": {"type": "string"},
                    "token": {"type": "string"},
                }
            },
        },
    }

    def load(self):
        config_path = get_app_file_path(CONFIG_FILE_NAME)

        logger.info(f"Load config from '{config_path}'")

        # Load config from file
        data = None
        if config_path.exists() and config_path.is_file():
            with config_path.open() as config_file:
                data = json.load(config_file)

        # Load settings
        if data is not None:
            # Validate by JSON schema
            jsonschema.validate(data, self._configSchema)

            # Application settings
            self.first_date_of_week = data["application"]["firstDateOfWeek"]

            # Jira settings
            self.jira.host = data["jira"]["host"]
            self.jira.user = data["jira"]["user"]
            self.jira.token = data["jira"]["token"]

            # Tempo settings
            self.tempo.token = data["tempo"]["token"]

            # Toggl settings
            self.toggl.host = data["toggl"]["host"]
            self.toggl.token = data["toggl"]["token"]
            self.toggl.user_agent = data["toggl"]["user_agent"]

    def save(self):
        config_dir = get_app_directory_path()
        config_dir.mkdir(exist_ok=True)

        config_path = get_app_file_path(CONFIG_FILE_NAME)

        logger.info(f"Save config to '{config_path}'")

        data = {
            "application": {
                "firstDateOfWeek": self.first_date_of_week,
            },
            "jira": {
                "host": self.jira.host,
                "user": self.jira.user,
                "token": self.jira.token
            },
            "tempo": {
                "token": self.tempo.token,
            },
            "toggl": {
                "host": self.toggl.host,
                "user_agent": self.toggl.user_agent,
                "token": self.toggl.token,
            }
        }

        with config_path.open(mode="w") as config_file:
            json.dump(data, config_file, indent=4)
